Source: debci
Section: devel
Priority: optional
Maintainer: Debian CI team <team+ci@tracker.debian.org>
Uploaders: Antonio Terceiro <terceiro@debian.org>, Paul Gevers <elbrus@debian.org>,
Build-Depends: debhelper (>= 9.20160709),
               amqp-tools,
               autopkgtest (>= 5.5~) <!pkg.debci.runtime>,
               bsdmainutils,
               curl,
               dctrl-tools,
               debian-archive-keyring,
               devscripts,
               fonts-font-awesome (>= 4.0.3~),
               inotify-tools <!pkg.debci.runtime>,
               jq,
               libjs-bootstrap,
               libjs-jquery,
               libjs-jquery-flot,
               moreutils,
               patchutils,
               rabbitmq-server <!pkg.debci.runtime>,
               rerun <!pkg.debci.runtime>,
               rsync,
               ruby,
               ruby-activerecord,
               ruby-bunny,
               ruby-foreman <!pkg.debci.runtime>,
               ruby-rack-test <!pkg.debci.runtime>,
               ruby-rspec (>= 2.14) <!pkg.debci.runtime>,
               ruby-sinatra,
               ruby-sinatra-contrib,
               ruby-sqlite3 <!pkg.debci.runtime>,
               ruby-thor,
               shellcheck <!pkg.debci.runtime>,
               shunit2 <!pkg.debci.runtime>,
               yard <!pkg.debci.runtime>,
Standards-Version: 4.3.0
Homepage: http://ci.debian.net/
Vcs-Git: https://salsa.debian.org/ci-team/debci.git
Vcs-Browser: https://salsa.debian.org/ci-team/debci

Package: debci
Architecture: all
Depends: adduser,
         debootstrap,
         netcat-openbsd | netcat-traditional,
         ruby-sqlite3 | ruby-pg,
         sudo,
         ${debci:Depends},
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: ntp | time-daemon
Suggests: apt-cacher-ng,
Description: continuous integration system for Debian
 debci will scan the Debian archive for packages that contain DEP-8 compliant
 test suites, and run those test suites whenever a new version of the package,
 or of any package in its dependency chain (modulo the base system), is
 available.
 .
 The requests are distributed to worker machines through AMQP queues. You need
 rabbitmq-server for this; but it is also possible to run RabbitMQ on a
 different server than debci, in which case you do not need to install that
 recommendation.
 .
 This package contains the debci core.

Package: debci-worker
Architecture: all
Depends: debci (= ${binary:Version}),
         autodep8 (>= 0.2~),
         autopkgtest (>= 5.5~),
         lxc | schroot,
         ${misc:Depends},
         ${shlibs:Depends}
Description: continuous integration system for Debian (worker daemon)
 debci will scan the Debian archive for packages that contain DEP-8 compliant
 test suites, and run those test suites whenever a new version of the package,
 or of any package in its dependency chain (modulo the base system), is
 available.
 .
 The requests are distributed to worker machines through AMQP queues. You need
 rabbitmq-server for this; but it is also possible to run RabbitMQ on a
 different server than debci, in which case you do not need to install that
 recommendation.
 .
 This package provides cron and init integration for running debci worker
 daemons that will listen to AMQP queues and run test jobs.

Package: debci-collector
Architecture: all
Depends: debci (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: rabbitmq-server,
Description: continuous integration system for Debian (collector daemon)
 debci will scan the Debian archive for packages that contain DEP-8 compliant
 test suites, and run those test suites whenever a new version of the package,
 or of any package in its dependency chain (modulo the base system), is
 available.
 .
 The requests are distributed to worker machines through AMQP queues. You need
 rabbitmq-server for this; but it is also possible to run RabbitMQ on a
 different server than debci, in which case you do not need to install that
 recommendation.
 .
 This package provides the collector daemon, which will receive test results
 published by debci worker hosts, store them centrally, and generate the static
 HTML files for the debci user interface.
